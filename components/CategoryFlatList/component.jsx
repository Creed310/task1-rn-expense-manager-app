import { View, Text } from 'react-native'
import React from 'react'

const CategoryFlatList = (props) => {

    const type = props.type
    const item = props.item

    const [categoryTextColor, valueTextColor, backgroundColour, sign] = type === "expense" ? ["#F13939", "#FC5B3E", "#F2DAC9", "-"] : ["#38C422", "#82C422","#E1F2C9", "+"]

  return (
    <View>
        <View style = {{backgroundColor: backgroundColour, padding: 10}}>
            <Text style = {{fontSize: 20, color: categoryTextColor, fontWeight: '400'}}>{item}</Text>
        </View>
    </View>
  )
}

export default CategoryFlatList