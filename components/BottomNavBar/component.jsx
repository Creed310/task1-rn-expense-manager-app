import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import Transactions_Bottom_Logo from '../../assets/39-397009_transparent-stock-speech-box-png-image-expense-icon.png-removebg-preview.png'
import Categories_Bottom_Logo from '../../assets/img_87908-removebg-preview.png'
import Settings_Logo from '../../assets/6002fa8551c2ec00048c6c79-removebg-preview.png'
// EXPENSE/INCOME HOME SCREEN, VIEW EI CATEGORIES, SETTINGS
const BottomNavBar = (props) => 
{
    const navigation = props.navigation
    // const onPressS = props.onPressS
    

  return (
    <View>
        <View style = 
            {{backgroundColor: '#fcfcfc',
            borderColor: '#E3DFDE',
            borderTopWidth: 1,

               height: 75}}>
            <Text></Text>
            <View style = {
                {
                flex: 1,
                backgroundColor: 'white',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 20,
                }}> 

                <View style = {
                    {
                        backgroundColor: 'white', 
                        flex: 1, 
                        flexDirection: 'row',
                        justifyContent: 'center'}}>
                        <View style = {{
                                            backgroundColor: 'white',
                                            flexDirection: 'column',
                                            justifyContent: 'center', 
                                            alignItems: 'center',
                                        }}> 
                                        
                            <View style = {{backgroundColor: 'white'}}>
                                    <TouchableOpacity 
                                        onPress = {() => 
                                            {
                                                navigation.navigate('Home')
                                            }}
                                        style = {{justifyContent: 'center', alignItems: 'center'}}>
                                        <Image 
                                            style = {{height: 40, width: 40, justifyContent: 'center', alignItems: 'center'}}
                                            source = {Transactions_Bottom_Logo}/>
                                        <Text style = {{textAlign: 'center', fontSize: 12, fontWeight: '300'}}>Transactions</Text>
                                    </TouchableOpacity>
                            </View>
                        </View>
                </View>

                <View style = {
                    {
                        backgroundColor: 'white', 
                        flex: 1, 
                        flexDirection: 'row',
                        justifyContent: 'center'}}>
                        <View style = {{
                                            backgroundColor: 'white',
                                            flexDirection: 'column',
                                            justifyContent: 'space-evenly', 
                                            alignItems: 'center',
                                        }}> 
                                        
                            <View style = {{backgroundColor: 'white'}}>
                            <TouchableOpacity 
                                    onPress = {() => navigation.navigate('ViewAllCategory')}
                                    style = {{paddingVertical: 15, justifyContent: 'center', alignItems: 'center'}}>
                                        <Image 
                                            style = {{height: 38, width: 38, justifyContent: 'center', alignItems: 'center'}}
                                            source = {Categories_Bottom_Logo}/>
                                        <Text style = {{textAlign: 'center', fontSize: 12, fontWeight: '300'}}>Categories</Text>
                                    </TouchableOpacity>
                            </View>
                        </View>
                </View>

                <View style = {
                    {
                        backgroundColor: 'white', 
                        flex: 1, 
                        flexDirection: 'row',
                        justifyContent: 'center'}}>
                        <View style = {{
                                            backgroundColor: 'white',
                                            flexDirection: 'column',
                                            justifyContent: 'center', 
                                            alignItems: 'center',
                                        }}> 
                                        
                            <View style = {{backgroundColor: 'white'}}>
                            <TouchableOpacity style = {{paddingVertical: 11, justifyContent: 'center', alignItems: 'center'}}>
                                        <Image 
                                            style = {{height: 40, width: 40, justifyContent: 'center', alignItems: 'center'}}
                                            source = {Settings_Logo}/>
                                        <Text style = {{textAlign: 'center', fontWeight: '300', fontSize: 12}}>Settings</Text>
                                    </TouchableOpacity>
                            </View>
                        </View>
                </View>
                
            </View>    
        </View>
    </View>
  )
}

export default BottomNavBar