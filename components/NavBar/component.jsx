import { StyleSheet, View, Image, TouchableOpacity } from 'react-native'
import React from 'react'


const NavBar = (props) => 
{
  const onPressLeft = props.onPressLeft
  const onPressRight = props.onPressRight
  const iconLeft = props.iconLeft
  const iconRight = props.iconRight
  const iconLeftStyle = props.iconLeftStyle
  const iconRightStyle = props.iconRightStyle

  return (
    <View style = {
      {
        backgroundColor: 'white',
        height: 100,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
      }}>
      <View style = {
        {
          flexDirection: 'row',
          flex: 1, 
          justifyContent: 'space-between',
          marginHorizontal: 10,
          marginBottom: 10
        }}>

      <TouchableOpacity onPress = {() => onPressLeft()}>
        <Image source = {iconLeft} style = {iconLeftStyle}/>
      </TouchableOpacity>
      
      <TouchableOpacity onPress = {() => onPressRight()}>
        <Image source = {iconRight} style = {iconRightStyle} />
      </TouchableOpacity>
      </View>
    </View>
  )
}

export default NavBar
