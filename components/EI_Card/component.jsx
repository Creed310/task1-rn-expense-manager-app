import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'

const EI_Card = (props) => {

    const type = props.type
    const category = props.category
    const value = props.value
    const date_of_entry = props.date_of_entry
    const id = props.id
    const navigation = props.navigation 
    const onPress = props.onPress

    const [typeText, categoryTextColor, valueTextColor, backgroundColour, sign] = type === "expense" ? ["Expense", "#F13939", "#FC5B3E", "#F2DAC9", "-"] : ["Income", "#38C422", "#82C422","#E1F2C9", "+"]


  return (
    <TouchableOpacity onPress = {() => onPress()}>
    <View style = 
        {{backgroundColor: 'white', 
            borderWidth: 1, 
            borderRadius: 5, 
            borderColor: '#8d8e8f',
            flexDirection: 'column',
            justifyContent: 'space-between', 
            margin: 5, 
            marginHorizontal: 15,  
            padding: 20}}>
                <View>
                    <View style = {{backgroundColor: 'white', marginRight: 20, flexDirection: 'row', justifyContent: 'space-between'}}>
                        
                        <View style = {{flex: 1, backgroundColor: 'white', width: 150, flexDirection: 'column'}}>
                            <View style = {{flex: 1, flexDirection: 'column'}}>
                                <Text style = {styles.textHeader} >Category</Text>
                                <Text style = {styles.textBody}>{category}</Text>
                            </View>
                        </View>

                        <View style = {{flex: 0.35, backgroundColor: backgroundColour, height: 20, justifyContent: 'center', alignItems: 'center', borderRadius: 20}}>
                            <Text style = {{fontWeight: '200'}}> {typeText} </Text>
                        </View>
                    </View>
                    

                    <View style = {{flex: 1, marginVertical: 20, backgroundColor: 'white', flexDirection: 'row', marginRight: 10, justifyContent: 'space-between'}}>
                        <View style = {{flex: 1, backgroundColor: 'white'}}>
                            <Text style = {styles.textHeader}>Amount</Text>
                            <Text style = {{fontWeight: '300'}}>₹{value} INR</Text>
                        </View>

                        <View style = {{flex: 0.4, backgroundColor: 'white'}}>
                            <Text style = {styles.textHeader}>Date</Text>
                            <Text style = {{fontWeight: '300'}}>{date_of_entry}</Text>
                        </View>
                    </View>
                </View>
        </View>
    </TouchableOpacity>
    
  )
}

const styles = StyleSheet.create({
    textHeader: 
    {
        fontWeight: '400', 
        color: "#97999c"
    },
    textBody:
    {
        fontWeight: 'bold'
    }
})
export default EI_Card