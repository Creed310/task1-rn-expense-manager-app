import { View, Text, SafeAreaView } from 'react-native'
import { React } from 'react'
import Modal from 'react-native-modal'
import { TouchableOpacity } from 'react-native-gesture-handler'

const ExportOptionsModal = (props) => {

  const ModalVisibility = props.ModalVisibility
  const setModalVisibility = props.setModalVisibility

  const exportAllCSV = props.exportAllCSV
  const exportIncomeCSV = props.exportIncomeCSV
  const exportExpenseCSV = props.exportExpenseCSV

  const exportTypeFilter = props.exportTypeFilter

  const exportAllPDF = props.exportAllPDF
  const exportIncomePDF = props.exportIncomePDF
  const exportExpensePDF = props.exportExpensePDF

  return (
    <Modal isVisible={ModalVisibility} animationIn='slideInUp' transparent={true} onBackdropPress = {() => setModalVisibility(false)}>
          <SafeAreaView
            style={{
              marginTop: "auto",
              justifyContent: 'center',
              alignItems: 'center',
              height: "25%",
              backgroundColor: "#E3DFDE",
              borderRadius: 15,
              borderColor: '#E3DFDE',

            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                backgroundColor: "white",
                width: "100%",
                borderColor: '#E3DFDE',
                borderWidth: 0.2,
                borderRadius: 15,
                alignItems: 'center'
              }}
            >
              <View style = {
                { 
                  borderTopLeftRadius: 15, 
                  borderTopRightRadius: 15, 
                  borderWidth: 0.1, 
                  flex: 0.5, 
                  flexDirection: 'row',
                  justifyContent: 'center', 
                  alignItems: 'center', 
                  width: '100%',
                  borderColor: '#E3DFDE',
                  backgroundColor: '#ECE9E8'}}>
                <Text style={{textAlign: 'center', fontSize: 18, padding: 25, fontWeight: '300'}}>
                  Choose an export option
                </Text>
              </View>

              <View style = {{flex: 1, backgroundColor: '#FCFBFB', justifyContent: 'center', flexDirection: 'column', alignItems: 'center', width: '100%', borderWidth: 0.2, borderColor: '#E3DFDE',}}>
                <View style = {{flex:1}}>
                  <TouchableOpacity 
                  style = {{flex: 1, justifyContent: 'center', alignItems: 'center', borderWidth: 0.1, borderColor: '#E3DFDE',width: '100%', backgroundColor: '#FCFBFB'}}
                  onPress = {() =>
                  {
                    if (exportTypeFilter == 'all')
                    {
                      exportAllCSV()
                    } 
                    else if (exportTypeFilter == 'expense')
                    {
                      exportExpenseCSV()
                    }
                    else if(exportTypeFilter == 'income')
                    {
                      exportIncomeCSV()
                    }
                  }}>
                      <Text style = {{fontSize: 20, fontWeight: '200', color: '#066b21', paddingHorizontal: 111}}>Export as Excel</Text>
                  </TouchableOpacity>
                </View>
                <View style = {{flex:1}}>
                  <TouchableOpacity 
                  style = {{flex: 1, justifyContent: 'center', alignItems: 'center', borderWidth: 0.1, borderColor: '#E3DFDE',width: '100%', backgroundColor: '#FCFBFB'}}
                  onPress = {() =>
                    {
                      if (exportTypeFilter == 'all')
                      {
                        exportAllPDF()
                      } 
                      else if (exportTypeFilter == 'expense')
                      {
                        exportExpensePDF()
                      }
                      else if(exportTypeFilter == 'income')
                      {
                        exportIncomePDF()
                      }
                    }}>
                      <Text style = {{fontSize: 20, fontWeight: '200', color: '#066b21', paddingHorizontal: 115}}>Export as PDF</Text>
                  </TouchableOpacity>
                </View>
      
              </View>
            </View>
          </SafeAreaView>
    </Modal>
  )
}

export default ExportOptionsModal

