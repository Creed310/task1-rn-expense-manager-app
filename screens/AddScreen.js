import { View, Text, Image, SafeAreaView, TextInput } from "react-native";
import React from "react";
import { useEffect, useState } from "react";
import RNDateTimePicker from "@react-native-community/datetimepicker";
//import { openDatabase } from 'react-native-sqlite-storage'

import NavBar from "../components/NavBar/component";
const db = SQLite.openDatabase("TISETApp.db");
import BottomNavBar from "../components/BottomNavBar/component";
import SelectDropdown from "react-native-select-dropdown";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";

import * as SQLite from "expo-sqlite";

// for RNDateTimePicker tutorial https://www.youtube.com/watch?v=Imkw-xFFLeE

const AddScreen = ({ navigation, route }) => {
  const type = route.params.type;

  // declaring date state
  const [DOE, setDOE] = useState(new Date());

  // declaring a formatted date state
  const [fDOE, setfDOE] = useState(
    DOE.getDate() + "-" + (DOE.getMonth() + 1) + "-" + DOE.getFullYear()
  );

  // text and colour formatting based on type
  const AddScreenText = `You're adding an ${type}.`;
  const ei_value_text = type === "expense" ? "spent" : "earned";
  const AlertScreenText =
    type === "expense" ? "Expense added." : "Income added.";
  const top_background_color = type === "expense" ? "#F2DAC9" : "#E1F2C9";

  // declaring value and category states for new record
  const [value, setValue] = useState(0);
  const [category, setCategory] = useState("");

  // setting the category flatlist which is used to choose from in the drop down
  const [categoryFL, setCategoryFL] = useState([]);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || new Date();
    let tempDate = new Date(currentDate);
    setDOE(currentDate);
    let fDate =
      tempDate.getDate() +
      "-" +
      (tempDate.getMonth() + 1) +
      "-" +
      tempDate.getFullYear();
    setfDOE(fDate);
  };

  // IMPORTANT: adding an expense/income
  const addEI = () => {
    if (!value) {
      alert("You need to enter a value.");
      return;
    }

    if (!category) {
      alert("You need to select a category.");
      return;
    }

    db.transaction((trx) => {
      trx.executeSql(
        "INSERT INTO exp_inc_table (type, value, category, date_of_entry) VALUES (?, ?, ?, ?);",
        [type, value, category, fDOE],
        () => {
          alert(AlertScreenText);
          navigation.pop();
        },
        () => {
          alert(`Could not add ${type}.`);
          navigation.pop();
        }
      );
    });
  };

  // displaying the category dropdown
  const view_category_menu = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT category FROM category_type_table WHERE type = ?;",
          [type],
          (tx, res) => {
            const category_menu = [];
            for (let i = res.rows.length - 1; i >= 0; i--) {
              category_menu.push(res.rows.item(i).category);
            }
            setCategoryFL(category_menu);
          },
          () => {
            console.log("cannot view");
          }
        );
      });
    });
  };

  // run view the category menu on reaching the page and refresh it
  // every time there is a change in the category flatlist
  useEffect(() => {
    view_category_menu();
  }, [categoryFL]);

  return (
    <View style={{ flex: 1, backgroundColor: "grey" }}>
      <NavBar
        navigation={navigation}
        onPressLeft={() => navigation.pop()}
        onPressRight={() => {
          addEI();
        }}
        iconRight={require("../assets/add_FILL0_wght100_GRAD0_opsz40.png")}
        iconLeft={require("../assets/arrow_back_ios_FILL0_wght100_GRAD0_opsz24.png")}
      />

      <View
        style={{
          backgroundColor: top_background_color,
          flexDirection: "column",
          alignItems: "center",
          padding: 30,
        }}
      >
        <Text
          style={{
            fontSize: 25,
            textAlign: "center",
            fontWeight: "500",
          }}
        >
          {AddScreenText}
        </Text>
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: "column",
          backgroundColor: "white",
          justifyContent: "center",
        }}
      >
        <Text
          style={{
            fontSize: 20,
            fontWeight: "300",
            marginLeft: 25,
            marginTop: 40,
          }}
        >
          Select an {type} category.
        </Text>
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            flexDirection: "row",
            width: "100%",
            justifyContent: "space-evenly",
            alignItems: "center",
          }}
        >
          <SelectDropdown
            defaultButtonText="Select"
            disabled={false}
            onSelect={(category) => {
              setCategory(category);
            }}
            buttonStyle={{
              backgroundColor: "#edeff0",
              width: "75%",
            }}
            dropdownIconPosition="right"
            data={categoryFL}
          />

          <View style={{ backgroundColor: "white" }}>
            <Menu>
              <MenuTrigger>
                <Image
                  style={{ height: 25, width: 25 }}
                  source={require("../assets/17764.png")}
                />
              </MenuTrigger>

              <MenuOptions>
                <MenuOption
                  text="Add a category"
                  onSelect={() =>
                    navigation.navigate("AddCategory", { type: type })
                  }
                  style={{ padding: 15 }}
                />
                <MenuOption
                  text="Modify a category"
                  onSelect={() =>
                    navigation.navigate("ModifyCategory", { type: type })
                  }
                  style={{ padding: 15 }}
                />
                <MenuOption
                  text="Delete a category"
                  onSelect={() =>
                    navigation.navigate("DeleteCategory", { type: type })
                  }
                  style={{ padding: 15 }}
                />
              </MenuOptions>
            </Menu>
          </View>
        </View>
        <View
          style={{
            backgroundColor: "white",
            flexDirection: "column",
            flex: 1,
            justifyContent: "space-evenly",
          }}
        >
          <Text
            style={{
              fontSize: 20,
              marginLeft: 25,
              marginBottom: 0,
              fontWeight: "300",
            }}
          >
            How much have you {ei_value_text}?
          </Text>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              alignItems: "center",
              marginLeft: 25,
              marginBottom: 0,
              paddingTop: 25,
            }}
          >
            <Text
              style={{
                fontSize: 18,
                paddingRight: 18,
              }}
            >
              ₹
            </Text>
            <TextInput
              placeholder="Enter a value"
              onChangeText={(value) => {
                setValue(value);
              }}
              style={{
                backgroundColor: "#f0f2f5",
                borderWidth: 0.5,
                padding: 10,
                width: "50%",
                height: "100%",
              }}
            />
          </View>
        </View>
      </View>
      <View style={{ flex: 0.75, backgroundColor: "white" }}>
        <View style={{ flex: 1, backgroundColor: "white" }}>
          <View
            style={{
              marginHorizontal: 25,
              marginTop: 20,
              height: 40,
              backgroundColor: "white",
              justifyContent: "center",
            }}
          >
            <Text style={{ fontSize: 20, fontWeight: "300" }}>
              When did this {type} happen?
            </Text>
          </View>

          <View
            style={{
              backgroundColor: "white",
              height: 100,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start",
            }}
          >
            <RNDateTimePicker
              mode="date"
              style={{ width: 150 }}
              value={DOE}
              onChange={onChange}
            />
          </View>
        </View>
      </View>

      <SafeAreaView
        style={{
          backgroundColor: "white",
        }}
      >
        <BottomNavBar navigation={navigation} />
      </SafeAreaView>
    </View>
  );
};

export default AddScreen;
