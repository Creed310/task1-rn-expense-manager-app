import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
} from "react-native";
import React, { useEffect, useState } from "react";

// using expo packages as can't use react-native packages in an expo project.

// import "react-native-gesture-handler";

import NavBar from "../components/NavBar/component";
import EI_InfoBar from "../components/EI_InfoBar/component";
import BottomNavBar from "../components/BottomNavBar/component";
import EI_Card from "../components/EI_Card/component";
import ExportOptionsModal from "../components/ExportOptionsModal/component";

import * as Print from "expo-print";
import * as SQLite from "expo-sqlite";
import * as FileSystem from "expo-file-system";
import * as Sharing from "expo-sharing";

import XLSX from "xlsx";

const db = SQLite.openDatabase("TISETApp.db");

// let add_icon = require("../../assets/add_box_FILL0_wght100_GRAD0_opsz40.png");
// let add_fill_icon = require("../../assets/add_FILL0_wght100_GRAD0_opsz40.png");
// let back_icon = require("../../assets/arrow_back_ios_FILL0_wght100_GRAD0_opsz24.png");
// let filter_icon = require("../../assets/filter_alt_FILL0_wght100_GRAD0_opsz40.png");

// https://www.youtube.com/watch?v=ZdwfTY-FItE - exporting file system
// https://www.farhansayshi.com/post/how-to-save-files-to-a-device-folder-using-expo-and-react-native/

const HomeScreen = ({ navigation }) => {
  // flatlist expense income table that is used to display
  const [FlatListEITable, setFlatListEITable] = useState([]);

  // all transactions, expenses, incomes tables
  const [allTxTable, setAllTxTable] = useState([]);
  const [allExpenseTable, setAllExpenseTable] = useState([]);
  const [allIncomeTable, setAllIncomeTable] = useState([]);

  // export type filter = either 'all', 'expense' or 'income'
  const [exportTypeFilter, setExportTypeFilter] = useState("");

  // expense and income totals used on the EI_InfoBar
  const [ExpenseTotal, setExpenseTotal] = useState(0);
  const [IncomeTotal, setIncomeTotal] = useState(0);

  // modal visibility when export option is called
  const [ModalVisibility, setModalVisibility] = useState(false);

  // pushing to temporary array and setting the flatlist method.
  const pushToTempFLandSet = (res) => {
    const FLTemp = [];
    for (let i = res.rows.length - 1; i >= 0; i--) {
      FLTemp.push(res.rows.item(i));
    }
    setFlatListEITable(FLTemp);
  };

  // viewing all the transactions
  const viewAllTable = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * FROM exp_inc_table",
          [],
          (trx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No transactions found.");
            }
          },
          () => {}
        );
      });
    });
  };

  // viewing all the expenses
  const viewExpenseTable = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * FROM exp_inc_table WHERE type = 'expense';",
          [],
          (trx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No expenses found.");
            }
          },
          () => {}
        );
      });
    });
  };

  // viewing all the incomes
  const viewIncomeTable = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * FROM exp_inc_table WHERE type = 'income';",
          [],
          (trx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No incomes found.");
            }
          },
          () => {}
        );
      });
    });
  };

  // exporting all transactions to .csv
  const exportAllCSV = async () => {
    let AllTablePromise = new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * from exp_inc_table;",
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No rows exist.");
            }
          },
          () => {}
        );
      });
    });

    AllTablePromise.then((res) => {
      const AllTable = [];
      for (let i = 0; i < res.rows.length; i++) {
        AllTable.push(res.rows.item(i));
      }
      setAllTxTable(AllTable);
      return allTxTable;
    }).then(async (allTxTable) => {
      let wb = XLSX.utils.book_new();
      let ws = XLSX.utils.json_to_sheet(allTxTable);
      XLSX.utils.book_append_sheet(wb, ws, "All Transactions");
      const wb_out = await XLSX.write(wb, { type: "binary", bookType: "csv" });

      const AllTableCSVFileURI = `${FileSystem.documentDirectory}allTransactions.csv`;
      console.log(AllTableCSVFileURI);

      // const writeResult = await FileSystem.writeAsStringAsync(
      //   AllTableCSVFileURI,
      //   wb_out
      // );

      const UTI = "public.item";
      const shareResult = await Sharing.shareAsync(AllTableCSVFileURI, {
        UTI,
      });
      console.log(shareResult);
    });

    // writeFile(DownloadDirectoryPath + "./allTransactions.csv", wb_out, "ascii")
    //   .then((result) => {
    //     alert("Exported data successfuly");
    //   })
    //   .catch((e) => {
    //     console.log("Error when writing to the file", e);
    //   });
  };

  // exporting all the expenses to .csv
  const exportExpenseCSV = async () => {
    let ExpenseTablePromise = new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * from exp_inc_table WHERE type = 'expense';",
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No rows exist.");
            }
          },
          () => {}
        );
      });
    });

    ExpenseTablePromise.then((res) => {
      const ExpenseTable = [];
      for (let i = 0; i < res.rows.length; i++) {
        ExpenseTable.push(res.rows.item(i));
      }
      setAllExpenseTable(ExpenseTable);
      return ExpenseTable;
    }).then(async (ExpenseTable) => {
      let wb = XLSX.utils.book_new();
      let ws = XLSX.utils.json_to_sheet(ExpenseTable);
      XLSX.utils.book_append_sheet(wb, ws, "All Expenses");
      const wb_out = XLSX.write(wb, { type: "binary", bookType: "csv" });

      const ExpenseTableCSVFileURI = `${FileSystem.documentDirectory}allExpenses.csv`;
      console.log(ExpenseTableCSVFileURI);

      // FileSystem.writeAsStringAsync(ExpenseTableCSVFileURI, wb_out);

      const UTI = "public.item";
      const shareResult = await Sharing.shareAsync(ExpenseTableCSVFileURI, {
        UTI,
      });
    });

    // writeFile(DownloadDirectoryPath + "./allTransactions.csv", wb_out, "ascii")
    //   .then((result) => {
    //     alert("Exported data successfuly");
    //   })
    //   .catch((e) => {
    //     console.log("Error when writing to the file", e);
    //   });
  };

  // exporting all the incomes to .csv
  const exportIncomeCSV = async () => {
    let IncomeTablePromise = new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * from exp_inc_table WHERE type = 'income';",
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No rows exist.");
            }
          },
          () => {}
        );
      });
    });

    IncomeTablePromise.then((res) => {
      const IncomeTable = [];
      for (let i = 0; i < res.rows.length; i++) {
        IncomeTable.push(res.rows.item(i));
      }
      setAllIncomeTable(IncomeTable);
      return IncomeTable;
    }).then(async (IncomeTable) => {
      let wb = XLSX.utils.book_new();
      let ws = XLSX.utils.json_to_sheet(IncomeTable);
      XLSX.utils.book_append_sheet(wb, ws, "All Incomes");
      const wb_out = XLSX.write(wb, { type: "binary", bookType: "csv" });

      const IncomeTableCSVFileURI = `${FileSystem.documentDirectory}allIncomes.csv`;
      console.log(IncomeTableCSVFileURI);

      // FileSystem.writeAsStringAsync(IncomeTableCSVFileURI, wb_out);
      const UTI = "public.item";
      const shareResult = await Sharing.shareAsync(IncomeTableCSVFileURI, {
        UTI,
      });
    });

    // writeFile(DownloadDirectoryPath + "./allTransactions.csv", wb_out, "ascii")
    //   .then((result) => {
    //     alert("Exported data successfuly");
    //   })
    //   .catch((e) => {
    //     console.log("Error when writing to the file", e);
    //   });
  };

  // exporting all the transactions to pdf
  const exportAllPDF = async () => {
    let AllTablePromise = new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * from exp_inc_table;",
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No rows exist.");
            }
          },
          () => {}
        );
      });
    });

    AllTablePromise.then((res) => {
      const AllTable = [];
      for (let i = 0; i < res.rows.length; i++) {
        AllTable.push(res.rows.item(i));
      }
      setAllTxTable(AllTable);
      return allTxTable;
    }).then(async (allTxTable) => {
      const date = new Date();
      const dd = String(date.getDate()).padStart(2, "0");
      const mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      const yyyy = date.getFullYear();

      const today = dd + "-" + mm + "-" + yyyy;

      let wb = XLSX.utils.book_new();
      let ws = XLSX.utils.json_to_sheet(allTxTable);
      XLSX.utils.book_append_sheet(wb, ws, "All Transactions");
      const wb_out = await XLSX.write(wb, { type: "binary", bookType: "html" });

      const options = { html: wb_out };
      const pdf = await Print.printToFileAsync(options);

      console.log(pdf.uri);

      // tried using regex, instead reversing, finding the first occurance of / and then adding the .pdf tag

      // doesn't work when sharing/saving to device, can't change the URI

      const baseURL = pdf.uri.substring(
        0,
        pdf.uri.length - pdf.uri.split("").reverse().join("").indexOf("/")
      );

      const AllTransactionsPDFURL = `${baseURL}AllTransactions_${today}.pdf`;

      const copyOptions = { from: pdf.uri, to: AllTransactionsPDFURL };
      const FS = await FileSystem.copyAsync(copyOptions);

      //  const AllTableHTMLFileURI = `${FileSystem.documentDirectory}allTransactions.html`;
      // // console.log(AllTableHTMLFileURI);

      // const writeResult = await FileSystem.writeAsStringAsync(pdf.uri, wb_out);

      // this shares the file -> can save to device from here.

      const UTI = "public.item";
      const shareResult = await Sharing.shareAsync(AllTransactionsPDFURL, {
        UTI,
      });
      console.log(shareResult);
    });

    // writeFile(DownloadDirectoryPath + "./allTransactions.csv", wb_out, "ascii")
    //   .then((result) => {
    //     alert("Exported data successfuly");
    //   })
    //   .catch((e) => {
    //     console.log("Error when writing to the file", e);
    //   });
  };

  // exporting all the expenses to pdf
  const exportExpensePDF = async () => {
    let ExpenseTablePromise = new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * from exp_inc_table WHERE type = 'expense';",
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No rows exist.");
            }
          },
          () => {}
        );
      });
    });

    ExpenseTablePromise.then((res) => {
      const ExpenseTable = [];
      for (let i = 0; i < res.rows.length; i++) {
        ExpenseTable.push(res.rows.item(i));
      }
      setAllExpenseTable(ExpenseTable);
      return allExpenseTable;
    }).then(async (allExpenseTable) => {
      const date = new Date();
      const dd = String(date.getDate()).padStart(2, "0");
      const mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      const yyyy = date.getFullYear();

      const today = dd + "-" + mm + "-" + yyyy;

      let wb = XLSX.utils.book_new();
      let ws = XLSX.utils.json_to_sheet(allExpenseTable);
      XLSX.utils.book_append_sheet(wb, ws, "All Expenses");
      const wb_out = await XLSX.write(wb, { type: "binary", bookType: "html" });

      const options = { html: wb_out };
      const pdf = await Print.printToFileAsync(options);

      // tried using regex, instead reversing, finding the first occurance of / and then adding the .pdf tag

      // doesn't work when sharing/saving to device, can't change the URI

      const baseURL = pdf.uri.substring(
        0,
        pdf.uri.length - pdf.uri.split("").reverse().join("").indexOf("/")
      );

      const AllExpensesPDFURL = `${baseURL}AllExpenses_${today}.pdf`;

      const copyOptions = { from: pdf.uri, to: AllExpensesPDFURL };
      const FS = await FileSystem.copyAsync(copyOptions);

      //  const AllTableHTMLFileURI = `${FileSystem.documentDirectory}allTransactions.html`;
      // // console.log(AllTableHTMLFileURI);

      // const writeResult = await FileSystem.writeAsStringAsync(pdf.uri, wb_out);

      // this shares the file -> can save to device from here.

      const UTI = "public.item";
      const shareResult = await Sharing.shareAsync(AllExpensesPDFURL, {
        UTI,
      });
      console.log(shareResult);
    });
  };

  // exporting all the incomes to pdf
  const exportIncomePDF = async () => {
    let IncomeTablePromise = new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * from exp_inc_table WHERE type = 'income';",
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              alert("No rows exist.");
            }
          },
          () => {}
        );
      });
    });

    IncomeTablePromise.then((res) => {
      const IncomeTable = [];
      for (let i = 0; i < res.rows.length; i++) {
        IncomeTable.push(res.rows.item(i));
      }
      setAllIncomeTable(IncomeTable);
      return allIncomeTable;
    }).then(async (allIncomeTable) => {
      const date = new Date();
      const dd = String(date.getDate()).padStart(2, "0");
      const mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
      const yyyy = date.getFullYear();

      const today = dd + "-" + mm + "-" + yyyy;

      let wb = XLSX.utils.book_new();
      let ws = XLSX.utils.json_to_sheet(allIncomeTable);
      XLSX.utils.book_append_sheet(wb, ws, "All Incomes");
      const wb_out = await XLSX.write(wb, { type: "binary", bookType: "html" });

      const options = { html: wb_out };
      const pdf = await Print.printToFileAsync(options);

      // tried using regex, instead reversing, finding the first occurance of / and then adding the .pdf tag

      // doesn't work when sharing/saving to device, can't change the URI

      const baseURL = pdf.uri.substring(
        0,
        pdf.uri.length - pdf.uri.split("").reverse().join("").indexOf("/")
      );

      const AllIncomesPDFURL = `${baseURL}AllIncomes_${today}.pdf`;

      const copyOptions = { from: pdf.uri, to: AllIncomesPDFURL };
      const FS = await FileSystem.copyAsync(copyOptions);

      //  const AllTableHTMLFileURI = `${FileSystem.documentDirectory}allTransactions.html`;
      // // console.log(AllTableHTMLFileURI);

      // const writeResult = await FileSystem.writeAsStringAsync(pdf.uri, wb_out);

      // this shares the file -> can save to device from here.

      const UTI = "public.item";
      const shareResult = await Sharing.shareAsync(AllIncomesPDFURL, {
        UTI,
      });
      console.log(shareResult);
    });
  };

  // running on startup
  useEffect(() => {
    // returning a promise of viewing all transactions and setting the flatlist
    viewAllTable().then((res) => pushToTempFLandSet(res));

    // finding the income total
    find_total_income().then((res) => {
      setIncomeTotal(res.rows.item(0).total_income);
    });

    // finding the expense total
    find_total_expense().then((res) => {
      setExpenseTotal(res.rows.item(0).total_expense);
    });

    // the flatlist is not being updated when a new transaction is added,
    // so when you switch tabs only it's getting updated.

    // flatlist should keep getting updated in useEffect, keep checking for new values
    // it is not what causes the change, it is what is changed.

    // NEEDS TO RUN EVERYTIME SOMETHING IS ADDED BUT CAN'T BE FLATLISTEITABLE BECAUSE TOO JITTERY
    // console.log("changes,", FlatListEITable)
    // QUESTION - used FlatListEITable here as the variable to check when refreshing, but when console logging it, it gets called repeatedly too fast, better way to solve?
  }, [navigation]);

  useEffect(() => {
    // focus - navigation event that is emitted when the screen comes into focus
    // this is so that everytime the app reaches this page, it runs the code in the callback function
    // .addListener returns a function that can be called to unsubscribe from the event

    const unsubscribe = navigation.addListener("focus", () => {
      viewAllTable().then((res) => pushToTempFLandSet(res));

      find_total_income().then((res) => {
        setIncomeTotal(res.rows.item(0).total_income);
      });

      find_total_expense().then((res) => {
        setExpenseTotal(res.rows.item(0).total_expense);
      });
    });

    return unsubscribe;
  }, [navigation]);

  // finding the total income using sum() in sql
  const find_total_income = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          'SELECT SUM(value) AS total_income FROM exp_inc_table WHERE type = "income";',
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              console.log("could not find total income value");
            }
          }
        ),
          () => {
            reject("could not find the total income value");
          };
      });
    });
  };

  // finding the total expense using sum() in sql
  const find_total_expense = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          'SELECT SUM(value) AS total_expense FROM exp_inc_table WHERE type = "expense";',
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              console.log("could not find total income value");
            }
          }
        ),
          () => {
            reject("could not find the total expense value");
          };
      });
    });
  };

  //Home Nav Bar -------------------
  return (
    <View style={{ flex: 1 }}>
      <NavBar navigation={navigation} />
      <EI_InfoBar
        navigation={navigation}
        expense_total={ExpenseTotal}
        income_total={IncomeTotal}
      />

      <View
        style={{
          flexDirection: "row",
          backgroundColor: "white",
          paddingHorizontal: 5,
        }}
      >
        <ExportOptionsModal
          setModalVisibility={() => setModalVisibility()}
          ModalVisibility={ModalVisibility}
          exportTypeFilter={exportTypeFilter}
          exportAllCSV={() => exportAllCSV()}
          exportIncomeCSV={() => exportIncomeCSV()}
          exportExpenseCSV={() => exportExpenseCSV()}
          exportAllPDF={() => exportAllPDF()}
          exportIncomePDF={() => exportIncomePDF()}
          exportExpensePDF={() => exportExpensePDF()}
        />

        <View
          style={{
            flexDirection: "row",
            marginHorizontal: 40,
            padding: 10,
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {/* View All Transactions */}
          <TouchableOpacity
            onLongPress={() => {
              setExportTypeFilter("all");
              setModalVisibility(true);
              // exportAllCSV()
            }}
            onPress={() =>
              viewAllTable().then((res) => pushToTempFLandSet(res))
            }
            style={{
              borderWidth: 0.2,
              backgroundColor: "#CEC4C2",
              padding: 10,
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View>
              <Text style={{ fontWeight: "200" }}>All</Text>
            </View>
          </TouchableOpacity>

          {/* View Expenses */}
          <TouchableOpacity
            onLongPress={() => {
              setExportTypeFilter("expense");
              setModalVisibility(true);
              //exportExpenseCSV()
            }}
            onPress={() =>
              viewExpenseTable().then((res) => pushToTempFLandSet(res))
            }
            style={{
              borderWidth: 0.2,
              backgroundColor: "#CEC4C2",
              padding: 10,
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View>
              <Text style={{ fontWeight: "200" }}>Expense</Text>
            </View>
          </TouchableOpacity>

          {/* View Incomes */}
          <TouchableOpacity
            onLongPress={() => {
              setExportTypeFilter("income");
              setModalVisibility(true);
              //exportIncomeCSV()
            }}
            onPress={() =>
              viewIncomeTable().then((res) => pushToTempFLandSet(res))
            }
            style={{
              borderWidth: 0.2,
              backgroundColor: "#CEC4C2",
              padding: 10,
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View>
              <Text style={{ fontWeight: "200" }}>Income</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>

      {/* Home Nav Bar */}
      <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
        <View style={{ flex: 1, backgroundColor: "white" }}>
          <FlatList
            data={FlatListEITable}
            renderItem={({ item }) => (
              <EI_Card
                navigation={navigation}
                id={item.id}
                type={item.type}
                category={item.category}
                value={item.value}
                date_of_entry={item.date_of_entry}
                onPress={() => {
                  navigation.navigate("RUD_EIScreen", { item: { item } });
                }}
              />
            )}
          ></FlatList>

          <BottomNavBar navigation={navigation} />
        </View>
      </SafeAreaView>
    </View>
  );
};

export default HomeScreen;
