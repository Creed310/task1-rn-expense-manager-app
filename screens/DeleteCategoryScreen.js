import { View, Text, SafeAreaView, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from "react";

//import { openDatabase } from 'react-native-sqlite-storage'
import BottomNavBar from "../components/BottomNavBar/component";
import NavBar from "../components/NavBar/component";

import SelectDropdown from "react-native-select-dropdown";

import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("TISETApp.db");

const DeleteCategoryScreen = ({ navigation, route }) => {
  const type = route.params.type;

  const top_background_color = type === "expense" ? "#F2DAC9" : "#E1F2C9";

  // flatlist state for displaying the dropdown with all the available categories
  const [oldCategoryFL, setOldCategoryFL] = useState([]);

  // state for the category to be deleted
  const [oldCategory, setoldCategory] = useState("");

  // state for the primary key of the category in the category-type table to be deleted
  const [ct_id, setCT_ID] = useState(0);

  // viewing the old category dropdown
  const view_old_categories = () => {
    db.transaction((trx) => {
      trx.executeSql(
        "SELECT ct_id, category FROM category_type_table WHERE type = ?;",
        [type],
        (tx, res) => {
          if (res.rows.length > 0) {
            const old_category_array = [];
            // console.log("result", res)
            for (let i = res.rows.length - 1; i >= 0; i--) {
              // console.log("getting pushed", res.rows.item(i).category)
              old_category_array.push(res.rows.item(i).category);
            }
            setOldCategoryFL(old_category_array);
          }
        },
        () => {
          console.log("could not find any categories");
        }
      );
    });
  };

  // getting the primary key of the category that is to be deleted
  const getCT_ID = () => {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          "SELECT ct_id FROM category_type_table WHERE category = ? AND type = ?;",
          [oldCategory, type],
          (trx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            }
          },
          () => {
            console.log("could not set CT_ID");
          }
        );
      });
    });
  };

  // deleting the category

  const delete_category = () => {
    db.transaction((trx) => {
      trx.executeSql(
        "DELETE FROM category_type_table WHERE ct_id = ?;",
        [ct_id],
        (tx, res) => {
          if (res.rowsAffected > 0) {
            alert("You have successfully deleted the category.");
            navigation.pop();
          } else {
            alert("Could not delete the category.");
            navigation.pop();
          }
        },
        () => {
          console.log("could not delete");
        }
      );
    });
  };

  // view all the old categories and get the primary key of the category
  // to be deleted every time it refreshes

  useEffect(() => {
    view_old_categories();

    getCT_ID().then((res) => setCT_ID(res.rows.item(0).ct_id));
  });

  return (
    <View style={{ flex: 1, backgroundColor: top_background_color }}>
      <NavBar
        navigation={navigation}
        iconLeft={require("../assets/arrow_back_ios_FILL0_wght100_GRAD0_opsz24.png")}
        onPressLeft={() => navigation.pop()}
      />

      <View
        style={{
          backgroundColor: top_background_color,
          flexDirection: "column",
          alignItems: "center",
          padding: 45,
        }}
      ></View>

      <View
        style={{
          flex: 0.5,
          flexDirection: "column",
          backgroundColor: "white",
          justifyContent: "flex-start",
          alignItems: "flex-start",
          paddingLeft: 20,
        }}
      >
        <Text
          style={{
            fontSize: 20,
            fontWeight: "300",
            marginVertical: 35,
          }}
        >
          Deleting an {type} category.
        </Text>

        <View
          style={{
            flexDirection: "row",
            flex: 0.5,
            height: "50%",
            backgroundColor: "white",
          }}
        >
          <SelectDropdown
            data={oldCategoryFL}
            onSelect={(oldCategory) => {
              setoldCategory(oldCategory);
            }}
            defaultButtonText="Select"
            buttonStyle={{
              width: "70%",
              height: "70%",
            }}
          />

          <TouchableOpacity
            style={{
              backgroundColor: top_background_color,
              marginHorizontal: 20,
              height: "70%",
              justifyContent: "center",
              paddingHorizontal: 10,
            }}
            onPress={() => {
              delete_category();
            }}
          >
            <Text> Delete </Text>
          </TouchableOpacity>
        </View>
      </View>

      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "white",
        }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: "#f5f5f5",
          }}
        ></View>
        <BottomNavBar navigation={navigation} />
      </SafeAreaView>
    </View>
  );
};

export default DeleteCategoryScreen;
