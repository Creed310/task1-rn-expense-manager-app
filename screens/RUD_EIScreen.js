import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
} from "react-native";

import React, { useEffect, useState } from "react";

import * as SQLite from "expo-sqlite";
import NavBar from "../components/NavBar/component";
import BottomNavBar from "../components/BottomNavBar/component";
const db = SQLite.openDatabase("TISETApp.db");

const RUD_EIScreen = ({ navigation, route }) => {
  // destructuring the route parameters into the respective fields
  const item = route.params.item.item;
  const type = item.type;
  const category = item.category;
  const value = item.value;
  const date_of_entry = item.date_of_entry;
  const rc_id = item.rc_id;

  const ei_value_text = type === "expense" ? "Expense" : "Income";
  const RUD_EIScreenText = `${ei_value_text} Details`;

  // states to maintain the state of the new value, new category
  // and new date to be changed

  const [valueChange, setValueChange] = useState(value);
  const [categoryChange, setCategoryChange] = useState(category);
  const [dateChange, setDateChange] = useState(date_of_entry);

  const text_color = type == "expense" ? "#FC5B3E" : "#82C422";

  // method to check if the category exists
  const does_category_exist = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * FROM category_type_table WHERE type = ? AND category = ?;",
          [type, categoryChange],
          (trx, res) => {
            resolve(res);
          },
          () => {
            reject("could not find.");
          }
        );
      });
    });
  };

  // method to update the category
  const update_category = () => {
    db.transaction((trx) => {
      trx.executeSql(
        "UPDATE exp_inc_table SET category = ? WHERE rc_id = ?;",
        [categoryChange, rc_id],
        () => {
          //console.log("category updated succesfully")
          alert("Category updated successfully.");
          navigation.pop();
        },
        () => {
          alert("Category could not be updated.");
          navigation.pop();
          //console.log('could not update category.')
        }
      );
    });
  };

  // method to update the value
  const update_value = () => {
    db.transaction((trx) => {
      trx.executeSql(
        "UPDATE exp_inc_table SET value = ? WHERE rc_id = ?;",
        [valueChange, rc_id],
        () => {
          //console.log("category updated succesfully")
          alert("Value updated successfully.");
          navigation.pop();
        },
        () => {
          alert("Value could not be updated.");
          navigation.pop();
          //console.log('could not update category.')
        }
      );
    });
  };

  // method to update the date
  const update_date = () => {
    db.transaction((trx) => {
      trx.executeSql(
        "UPDATE exp_inc_table SET date_of_entry = ? WHERE rc_id = ?;",
        [dateChange, rc_id],
        () => {
          //console.log("category updated succesfully")
          alert("Date updated successfully.");
          navigation.pop();
        },
        () => {
          alert("Date could not be updated.");
          navigation.pop();
          //console.log('could not update category.')
        }
      );
    });
  };

  // method to delete the expense/income
  const delete_EI = () => {
    db.transaction((trx) => {
      trx.executeSql(
        "DELETE from exp_inc_table WHERE rc_id = ?",
        [rc_id],
        (tx, res) => {
          if (res.rowsAffected > 0) {
            alert(`The ${type} has been deleted.`);
            navigation.pop();
          } else {
            alert(`Could not delete the ${type}.`);
          }
        }
      );
    });
  };
  useEffect(() => {
    console.log(item);
  });

  return (
    <View style={{ flex: 1, backgroundColor: "grey" }}>
      <NavBar
        navigation={navigation}
        iconLeft={require("../assets/arrow_back_ios_FILL0_wght100_GRAD0_opsz24.png")}
        iconRight={require("../assets/delete_FILL0_wght100_GRAD200_opsz40.png")}
        onPressRight={() => {
          delete_EI();
        }}
        onPressLeft={() => navigation.pop()}
      />

      <View
        style={{
          flex: 0.15,
          backgroundColor: "white",
          flexDirection: "column",
          alignItems: "center",
          padding: 30,
        }}
      >
        <Text
          style={{
            fontSize: 35,
            textAlign: "center",
            fontWeight: "400",
            color: text_color,
          }}
        >
          {RUD_EIScreenText}
        </Text>
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: "column",
          backgroundColor: "white",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              marginHorizontal: 20,
              flexDirection: "column",
            }}
          >
            <Text style={{ fontSize: 30, fontWeight: "200" }}>Value</Text>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "white",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{ fontWeight: "400", color: text_color, fontSize: 40 }}
                >
                  ₹
                </Text>
                <TextInput
                  onEndEditing={() => setValueChange(value)}
                  onChangeText={(newValue) => setValueChange(newValue)}
                  style={{ fontWeight: "300", color: text_color, fontSize: 40 }}
                >
                  <Text>{valueChange}</Text>
                </TextInput>
              </View>
              <TouchableOpacity onPress={() => update_value()}>
                <Image
                  style={{
                    width: 30,
                    height: 30,
                    paddingRight: 40,
                    marginRight: 10,
                  }}
                  source={require("../assets/add_box_FILL0_wght100_GRAD0_opsz40.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            backgroundColor: "white",
            justifyContent: "center",
            marginHorizontal: 20,
            height: "50%",
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <View>
              <Text style={{ fontSize: 30, fontWeight: "200" }}>Category</Text>
              <TextInput
                onEndEditing={() => setCategoryChange(category)}
                onChangeText={(newCategory) => setCategoryChange(newCategory)}
              >
                <Text
                  style={{ fontWeight: "300", color: text_color, fontSize: 40 }}
                >
                  {categoryChange}
                </Text>
              </TextInput>
            </View>
            <TouchableOpacity
              onPress={() => {
                does_category_exist().then((res) => {
                  if (res.rows.length > 0) {
                    //remove
                    // alert("The category exists.");
                    update_category();
                  } else {
                    alert("The category entered does not exist.");
                  }
                });
              }}
            >
              <Image
                style={{
                  width: 30,
                  height: 30,
                  paddingRight: 40,
                  marginRight: 10,
                }}
                source={require("../assets/add_box_FILL0_wght100_GRAD0_opsz40.png")}
              />
            </TouchableOpacity>
          </View>

          <View></View>
        </View>
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: "column",
          backgroundColor: "white",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            justifyContent: "center",
          }}
        >
          <View
            style={{
              height: "50%",
              marginHorizontal: 20,
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <View style={{ flexDirection: "column" }}>
              <Text style={{ fontSize: 30, fontWeight: "200" }}>
                Date of {ei_value_text}
              </Text>

              <TextInput
                onEndEditing={() => setDateChange(date_of_entry)}
                onChangeText={(newDate) => setDateChange(newDate)}
              >
                <Text
                  style={{ fontWeight: "300", color: text_color, fontSize: 40 }}
                >
                  {dateChange}
                </Text>
              </TextInput>
            </View>
            <TouchableOpacity onPress={() => update_date()}>
              <Image
                style={{
                  width: 30,
                  height: 30,
                  paddingRight: 40,
                  marginRight: 10,
                }}
                source={require("../assets/add_box_FILL0_wght100_GRAD0_opsz40.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 1, backgroundColor: "white" }}></View>
      </View>

      <SafeAreaView
        style={{
          backgroundColor: "white",
        }}
      >
        <BottomNavBar navigation={navigation} />
      </SafeAreaView>
    </View>
  );
};

export default RUD_EIScreen;
