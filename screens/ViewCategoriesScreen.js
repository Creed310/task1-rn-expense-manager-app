import { View, Text, SafeAreaView, FlatList } from "react-native";
import React, { useEffect, useState } from "react";
//import { openDatabase } from 'react-native-sqlite-storage'
import * as SQLite from "expo-sqlite";
import NavBar from "../components/NavBar/component";
const db = SQLite.openDatabase("TISETApp.db");
import BottomNavBar from "../components/BottomNavBar/component";
import CategoryFlatList from "../components/CategoryFlatList/component";
const ViewCategoriesScreen = ({ navigation }) => {
  // flatlists to store the categories of expenses and income
  const [ExpenseCategories, setExpenseCategories] = useState([]);
  const [IncomeCategories, setIncomeCategories] = useState([]);

  // render the category flatlist
  const renderCategoryFlatList = () => {
    return new Promise((resolve, reject) => {
      db.transaction((trx) => {
        trx.executeSql(
          "SELECT * from category_type_table;",
          [],
          (tx, res) => {
            if (res.rows.length > 0) {
              resolve(res);
            } else {
              reject("nothing found");
            }
          },
          () => {
            console.log("error");
          }
        );
      });
    });
  };

  useEffect(() => {
    renderCategoryFlatList().then((res) => {
      const ExpenseCategoriesFlatListData = [];
      const IncomeCategoriesFlatListData = [];

      for (let i = 0; i < res.rows.length; i++) {
        if (res.rows.item(i).type == "expense") {
          ExpenseCategoriesFlatListData.push(res.rows.item(i).category);
        } else if (res.rows.item(i).type == "income") {
          IncomeCategoriesFlatListData.push(res.rows.item(i).category);
        }
      }
      setExpenseCategories(ExpenseCategoriesFlatListData);
      setIncomeCategories(IncomeCategoriesFlatListData);
    });
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <NavBar
        navigation={navigation}
        onPressLeft={() => navigation.pop()}
        iconLeft={require("../assets/arrow_back_ios_FILL0_wght100_GRAD0_opsz24.png")}
      />
      <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
        <View style={{ flex: 1, backgroundColor: "green" }}>
          <View
            style={{
              flex: 0.2,
              backgroundColor: "white",
              justifyContent: "center",
            }}
          >
            <Text
              style={{ textAlign: "center", fontSize: 30, fontWeight: "300" }}
            >
              Categories
            </Text>
          </View>
          <View
            style={{ flex: 1, backgroundColor: "white", flexDirection: "row" }}
          >
            <View
              style={{
                flex: 1,
                backgroundColor: "white",
                marginRight: 5,
                marginLeft: 10,
              }}
            >
              <FlatList
                data={IncomeCategories}
                renderItem={({ item }) => (
                  <CategoryFlatList type="income" item={item} />
                )}
              />
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: "white",
                marginLeft: 5,
                marginRight: 10,
              }}
            >
              <FlatList
                data={ExpenseCategories}
                renderItem={({ item }) => (
                  <CategoryFlatList type="expense" item={item} />
                )}
              />
            </View>
          </View>
        </View>

        <BottomNavBar navigation={navigation} />
      </SafeAreaView>
    </View>
  );
};

export default ViewCategoriesScreen;
